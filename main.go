package main

import (
	"bufio"
	"fmt"
	"log"
	"net"
	"os"
	"strings"
	"sync"
	"time"
)

type SafeCounter struct {
	C   int
	Mux sync.Mutex
}

func (sf *SafeCounter) Add() {
	sf.Mux.Lock()
	defer sf.Mux.Unlock()

	sf.C++
}

func (sf *SafeCounter) Reset() {
	sf.Mux.Lock()
	defer sf.Mux.Unlock()

	sf.C = 0
}

type TwitchIrcChatLogger struct {
	Connection net.Conn
	Counter    SafeCounter
	Total      SafeCounter
	Targets    map[string]*os.File
}

func (irc *TwitchIrcChatLogger) Connect() (err error) {
	irc.Connection, err = net.Dial("tcp", IrcHost+":"+IrcPort)
	if err != nil {
		panic(err)
	}
	err = irc.SendMessage("PASS oauth:" + AccessToken)
	err = irc.SendMessage("NICK " + ChatUsername)
	return err
}

func (irc *TwitchIrcChatLogger) SendMessage(message string) (err error) {
	_, err = irc.Connection.Write([]byte(message + "\r\n"))
	return err
}

func (irc *TwitchIrcChatLogger) Listen() {
	scanner := bufio.NewScanner(irc.Connection)
	for scanner.Scan() {
		splitted := strings.Split(scanner.Text(), ":")

		if strings.Contains(splitted[1], "PING") {
			irc.SendMessage("PONG")
			fmt.Println("PONG")
			continue
		}

		spaceSplitted := strings.Split(splitted[1], " ")

		if spaceSplitted[1] == "PRIVMSG" {
			channel := spaceSplitted[2]

			log.SetOutput(irc.Targets[channel])

			message := splitted[2]

			var username string
			for i := 0; i < len(spaceSplitted[0]); i++ {
				if spaceSplitted[0][i] == byte('!') {
					username = spaceSplitted[0][:i]
				}
			}

			log.Println(username+":", message)

			irc.Counter.Add()
			irc.Total.Add()
		}
	}
}

func (irc *TwitchIrcChatLogger) Join(channel string) error {
	return irc.SendMessage("JOIN " + channel)
}

func (irc *TwitchIrcChatLogger) Leave(channel string) error {
	return irc.SendMessage("LEAVE #" + channel)
}

func (irc *TwitchIrcChatLogger) PrivMsg(channel, message string) error {
	return irc.SendMessage("PRIVMSG #" + channel + " :" + message)
}

func (irc *TwitchIrcChatLogger) StartLogger() {
	targets := []string{"mizkif", "greekgodx", "jonzherka", "rajjpatel", "xqcow"}

	irc.Targets = make(map[string]*os.File, len(targets))

	for i := 0; i < len(targets); i++ {
		err := irc.Join("#" + targets[i])
		if err != nil {
			panic(err)
		}

		f, err := os.OpenFile(targets[i]+".log", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
		if err != nil {
			panic(err)
		}

		irc.Targets["#"+targets[i]] = f
	}
}

func (irc *TwitchIrcChatLogger) Exit() {
	for _, value := range irc.Targets {
		value.Close()
	}
}

func main() {
	irc := TwitchIrcChatLogger{}
	defer irc.Exit()

	irc.Connect()
	go irc.Listen()

	irc.StartLogger()

	t := time.NewTicker(time.Second)

	for {
		<-t.C

		fmt.Printf(" Total: %d, Message/s: %d   \r", irc.Total.C, irc.Counter.C)

		irc.Counter.Reset()
	}
}
